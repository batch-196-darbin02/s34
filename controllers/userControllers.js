/*
	Naming convention for controllers is that is should be named after the model or documents it is concerned with.

	Controllers are functions which contain the actual business logic of our API and is triggered by a route.

	MVC : models, views, controllers. We will discover views on ReactJS.
*/

// To create a controller, we first add it to our module.exports so that we can import the controllers from our module.

// Import the User model in the controllers instead because this is where we are now going to use it.
const User = require("../models/User");

// Import bcrypt. bcrypt is a package which allows us to hash our passwords to add a layer of security for our user's details
const bcrypt = require("bcrypt");

// import auth.js module to use createAccessToken and its subsequent methods
const auth = require("../auth");

// userController.registerUser
module.exports.registerUser = (req,res) => {

	// console.log(req.body) // check the input passed via the client

	// Using bcrypt, we're going to hide the user's password underneath a layer of randomized characters. Salt rounds are the number of times we randomize the string/password hash
	//bcrypt.hashSync (<string>,<saltRounds>)
	const hashedPw = bcrypt.hashSync(req.body.password,10)
	console.log(hashedPw);

	let newUser = new User ({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


// getUserDetails should only allow the logged-in user to get his own details
module.exports.getUserDetails = (req,res) => {
	
	// console.log(req.user); // contains the details of the logged-in user

	// User.find({"_id": req.body._id})
	// .find() will return an array of documents which matches the criteria. It will return an array even if it only found 1 document.
	
	// User.findOne({"_id": req.body._id})
	// .findOne() will return a single document that matched our criteria.

	// findById() is a Mongoose method that will allows us to find a document strictly by its id.
	// This will allow us to ensure that the logged-in user or the user that passed the token will be able to get his own details and only his own
	User.findById(req.user._id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.loginUser = (req,res) => {

	console.log(req.body);

	/*
		Steps for logging in our user:
		1. Find the user by its email
		2. If we found the user, we will check his password if the password input and the hashed password in our database matches.
		3. If we don't find a user, we will send a message to the client.
		4. If upon checking, the found user's password is the same as our input password, we will generate a key for the user to have authorization to access certain features in our app.

	*/

	// translation to MondoDB: db.users.findOne({email:})
	User.findOne({email:req.body.email})
	.then(foundUser => {

		// foundUser is the parameter that contains the result of findOne
		// findOne() returns null if it is not able to match any document
		if(foundUser === null){
			return res.send({message: "No user found."});
		} else {
			// console.log(foundUser);
			// If we find a user, foundUser will contain the document that matched the input email.
			// Check if the input password from req.body matches the hashed password in our foundUser document
			/*
				bcrypt.compareSync(<inputString>,<hashedString>)

				"spidermanOG"
				$2a$10$dfuMrK/lFiIvNVLU0qIzIOLfj.pc3a2jxkGVgJoXqe8WLpKqr0X0q

				If the input string and hasedString matches and are the same, the compareSync method will return true.
				Else, it will return false.
			*/
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

			// console.log(isPasswordCorrect);

			// If the password is correct, we will create a "key", a token, for our user. Else, we will send a message:
			if(isPasswordCorrect){

				/*
					To be able to create a "key" or token that allows/authorizes our logged in user around our application, we have to create our module called auth.js

					This module will create an encoded string which contains our user's details.

					This encoded string is what we call a JSONWebToken or JWT.

					This JWT can only be properly unwrapped or decoded with our own secret word/string.
				*/

				// console.log("We will create a token for the user if the password is correct.");

				// auth.createAccessToken receives our foundUser document as an argument, gets only necessary details, wraps those details in JWT and our secret, then finally returns our JWT. This JWT will be sent to our client.
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			}
			else {
				return res.send({message: "Incorrect password."});
			}
		}


	})
};

module.exports.checkUserByEmail = (req,res) => {
	User.findOne({email:req.body.email})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};