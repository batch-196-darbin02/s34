/*
	Naming convention for controllers is that is should be named after the model or documents it is concerned with.

	Controllers are functions which contain the actual business logic of our API and is triggered by a route.

	MVC : models, views, controllers. We will discover views on ReactJS.
*/

// Import the Course model so we can manipulate it and add a new course document.
const Course = require("../models/Course");


module.exports.getAllCourses = (req,res) => {

	// Use the course model to connect to our collection and retrieve our courses.
	// To be able to query into our collections we use the model connected to that collection
	// in MongoDB: db.courses.find({})
	// Model.find() returns a collection of documents that matches our criteria similar to MongoDB's .find()
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.AddCourse = (req,res) => {
	
	// console.log(req.body);

	// using the Course model, we will use its constructor to create our Course document which will follow the schema of the model and add methods for document creation.

	let newCourse = new Course ({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	});

	// console.log(newCourse);
	// newCourse is now an object which follows the courseSchema and with additional methods from our Course constructor
	// .save method is added into our newCourse object. This will allow us to save the content of newCourse into our collection
	// db.courses.insertOne()

	// .then() allows us to process the result of a previous function/method in its own anonymous function
	// .catch() catches the error and allows us to process and send to the client
	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


module.exports.getSingleCourse = (req,res) => {

	Course.findById(req.body._id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}