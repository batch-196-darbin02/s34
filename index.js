const express = require("express");

// Mongoose is an ODM library to let our ExpressJS API manipulate a MongoDB database.
const mongoose = require("mongoose")

const app = express();

const port = 4000;

/*
	Mongoose Connection

	mongoose.connect() is a method to connect our API with our Mongodb database via the use of Mongoose. It has 2 arguments. First, is the connection string to connect our API to our MongoDB. Second, is an object used to add information between Mongoose and MongoDB

	replace/change the <password> in the connection string to your db user password.

	just before the ? in the connection string, add the database name.
*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.6dj5p.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// We will create notifications if the connection to the db (database) is a success or failed
let db = mongoose.connection;

db.on(`error`,console.error.bind(console,"MongoDB Connection Error.")) // This is done just to show notification of an internal server error from MongoDB.

// If the connection is open and successful, we will output a message in the terminal/gitbash.
db.once(`open`,() => console.log("Connected to MongoDB."))

// express.json() to be able to handle the request body and parse it into JS Objects
app.use(express.json());

/*
app.get('/courses', (req,res) => {
	res.send("This route will get all course documents.");
});

app.post('/courses', (req,res) => {
	res.send("This route will create a new course document.");
});
*/

// import our routes and use it as middleware.
// which means, that will be able to group together our routes
const courseRoutes = require('./routes/courseRoutes');
// console.log(courseRoutes);

// use our routes and group them together under '/courses'
// our endpoints are now prefaced with '/courses'
app.use('/courses',courseRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);


app.listen(port,() => console.log (`Server is running at localhost ${port}!`))