/*
	The naming convention for model files is the singular and capitalized form of the name of the document.
*/
const mongoose = require("mongoose");

/*
	Mongoose Schema

	Before we can create documents from our API to save into our database, we must first determine the structure of the document be written in the database.

	Schema acts as a blueprint for our data/document.

	A schema is a representation of how the document is structured. It also determines the types of data and the expected properties.

	So, with this, we won't have to worry if had input "stock" or "stocks" in our documents, because we can make it uniform with a schema.

	In Mongoose, to create a schema, we use the Schema() constructor from Mongoose. This will allow us to create a new schema object.
*/

	// 'new' keyword allows us to create a new object
	const courseSchema = new mongoose.Schema({
		
		name: {
			type: String,
			required: [true,"Name is required"]
		},
		description: {
			type: String,
			required: [true, "Description is required"]
		},
		price: {
			type: Number,
			required: [true, "Price is required"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		enrollees: [
			{
				userId: {
					type: String,
					required: [true, "User Id is required"]
				},
				dateEnrolled: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: "Enrolled"
				}
			}

		]

	});

	// module.exports - so we can import and use this file in another file.
	/*
		Mongoose Model is the connection to our collection.

		It has two arguments. First, the name of the collection model is going connect to. In MongoDB, once we create a new course document this model will connect to our courses collection. But since the courses collection is not yet created initially, MongoDB will create it for us.

		The second argument is the schema of the documents in the collection.
	*/
	module.exports = mongoose.model("Course",courseSchema)