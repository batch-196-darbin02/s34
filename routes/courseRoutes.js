/*
	To be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.

	The Router() method, will allow us to contain our routes

*/

const express = require("express");

const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");

const { verify,verifyAdmin } = auth;

// All routes to courses now has an endpoint prefaced with '/courses'
// endpoint - /courses/
// This route gets all course documents whether it is active or inactive
router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);

// endpoint - /courses/
// Only logged-in user is able to use addCourse
// verifyAdmin() will disallow regular logged-in and non-logged-in users from using route
router.post('/',verify,verifyAdmin,courseControllers.AddCourse);

// Get all active courses for regular and non-logged-in users
router.get('/activeCourses',courseControllers.getActiveCourses);

router.post('/getSingleCourse',courseControllers.getSingleCourse);


module.exports = router;